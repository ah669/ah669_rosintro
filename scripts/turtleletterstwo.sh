# !/ usr / bin / bash
# Make the letter A
rosservice call /spawn 0 4 0 "turtle2"
rosservice call /turtle1/set_pen 10 10 10 1 off
rosservice call /turtle2/set_pen 20 80 100 1 off
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
-- '[2.0 , 2.0 , 0.0]' '[0.0 , 0.0 , 0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
-- '[1.0 , -1.0 , 0.0]' '[0.0 , 0.0 , 0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
-- '[-2.0 , 0.0 , 0.0]' '[0.0 , 0.0 , 0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
-- '[2.0 , 0.0 , 0.0]' '[0.0 , 0.0 , 0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
-- '[1.0 , -1.0 , 0.0]' '[0.0 , 0.0 , 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
-- '[0.0 , 1.0 , 0.0]' '[0.0 , 0.0 , 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
-- '[-3.0 , 0.0 , 0.0]' '[0.0 , 0.0 , 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
-- '[6.0 , 0.0 , 0.0]' '[0.0 , 0.0 , 0.0]'
