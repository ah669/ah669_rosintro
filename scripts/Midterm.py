#Midterm Project Dr. Oca Intro to Robotics Fall 2023 Semester
#Modified by Athre Hollakal
#Kept in copyright use comments below for import tutorial code

#!/usr/bin/env python

# Software License Agreement (BSD License)
#

# Copyright (c) 2013, SRI International
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following
#    disclaimer in the documentation and/or other materials provided
#    with the distribution.
#  * Neither the name of SRI International nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# Author: Acorn Pooley, Mike Lautman

## imports
##
## To use the Python MoveIt interfaces, we will import the `moveit_commander`_ namespace.
## This namespace provides us with a `MoveGroupCommander`_ class, a `PlanningSceneInterface`_ class,
## and a `RobotCommander`_ class. More on these below. We also import `rospy`_ and some messages that we will use:
##

# Python 2/3 compatibility imports
from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi

    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

## END


#Defining my class as Midterm
class Midterm(object):
    """Midterm"""
    #Initialize method
    def __init__(self):
        super(Midterm, self).__init__()

        ## setup
        ##
        ## First initialize `moveit_commander`_ and a `rospy`_ node:
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node("midterm", anonymous=True)

        ## Instantiate a `RobotCommander`_ object. Provides information such as the robot's
        ## kinematic model and the robot's current joint states
        robot = moveit_commander.RobotCommander()

        ## Instantiate a `PlanningSceneInterface`_ object.  This provides a remote interface
        ## for getting, setting, and updating the robot's internal understanding of the
        ## surrounding world:
        scene = moveit_commander.PlanningSceneInterface()

        ## Instantiate a `MoveGroupCommander`_ object.  This object is an interface
        ## to a planning group (group of joints).
        ## This interface can be used to plan and execute motions:
        ## Name of UR5e is manipulator
        group_name = "manipulator"
        move_group = moveit_commander.MoveGroupCommander(group_name)

        ## Create a `DisplayTrajectory`_ ROS publisher which is used to display
        ## trajectories in Rviz:
        display_trajectory_publisher = rospy.Publisher(
            "/move_group/display_planned_path",
            moveit_msgs.msg.DisplayTrajectory,
            queue_size=20,
        )

        #Same code as the go_to_joint_state method
        #put the goal goals here so that the robot starts out moving out of a singularity
        #the default pose ideal for drawing letters
        ## BEGIN plan_to_joint_state
        ##
        ## Planning to a Joint Goal
        ## ^^^^^^^^^^^^^^^^^^^^^^^^
        ## We use the constant `tau = 2*pi <https://en.wikipedia.org/wiki/Turn_(angle)#Tau_proposals>`_ for convenience:
        # We get the joint values from the group and change some of the values:
        #This initial configuration is the easiest to write with
        joint_goal = move_group.get_current_joint_values()
        print(joint_goal)
        joint_goal[0] = 0
        joint_goal[1] = -tau/6    #upper arm joint move -tau/6
        joint_goal[2] = tau/8     #forearm joint move tau/8
        joint_goal[3] = tau/8     #wrist 1 joint move tau/8
        joint_goal[4] = 0
        joint_goal[5] = 0     
        print(joint_goal)

        # The go command can be called with joint values, poses, or without any
        # parameters if you have already set the pose or joint target for the group
        move_group.go(joint_goal, wait=True)
        ## END


        ## BEGIN basic_info
        ##
        ## Getting Basic Information
        ## ^^^^^^^^^^^^^^^^^^^^^^^^^
        # We can get the name of the reference frame for this robot:
        planning_frame = move_group.get_planning_frame()
        print("============ Planning frame: %s" % planning_frame)

        # We can also print the name of the end-effector link for this group:
        eef_link = move_group.get_end_effector_link()
        print("============ End effector link: %s" % eef_link)

        # We can get a list of all the groups in the robot:
        group_names = robot.get_group_names()
        print("============ Available Planning Groups:", robot.get_group_names())

        # Sometimes for debugging it is useful to print the entire state of the
        # robot:
        print("============ Printing robot state")
        print(robot.get_current_state())
        print("")
        ## END_SUB

        # Misc variables
        self.box_name = ""
        self.robot = robot
        self.scene = scene
        self.move_group = move_group
        self.display_trajectory_publisher = display_trajectory_publisher
        self.planning_frame = planning_frame
        self.eef_link = eef_link
        self.group_names = group_names

    #Function to return robot to a default pose ideal for drawing initials
    def go_to_joint_state(self):
        # Copy class variables to local variables
        move_group = self.move_group

        ## BEGIN plan_to_joint_state
        ##
        ## Planning to a Joint Goal
        ## ^^^^^^^^^^^^^^^^^^^^^^^^
        ## We use the constant `tau = 2*pi <https://en.wikipedia.org/wiki/Turn_(angle)#Tau_proposals>`_ for convenience:
        # We get the joint values from the group and change some of the values:
        #This initial configuration is the easiest to write with
        joint_goal = move_group.get_current_joint_values()
        print(joint_goal)
        joint_goal[0] = 0
        joint_goal[1] = -tau/6    #upper arm joint move -tau/6
        joint_goal[2] = tau/8     #forearm joint move tau/8
        joint_goal[3] = tau/8     #wrist 1 joint move tau/8
        joint_goal[4] = 0
        joint_goal[5] = 0     
        print(joint_goal)

        # The go command can be called with joint values, poses, or without any
        # parameters if you have already set the pose or joint target for the group
        move_group.go(joint_goal, wait=True)
        
        # Calling ``stop()`` ensures that there is no residual movement
        move_group.stop()

        ## END

    #helper method of plan_cartesian_path 
    #to take in 3 new parameters (in addition to the current pose; wpose) deltaX, deltaY, and deltaZ
    #which will be the change in XYZ position for the EE respectively to reach the new waypoint 
    #negative input for delta will move backwards in that direction, 0 input will not move in that direction
    #scale is set to 1
    def add_waypoint(self, wpose, deltaX, deltaY, deltaZ, scale=1):
        # Copy class variables to local variables
        move_group = self.move_group

        #take the pose position x and add the delta for the new waypoint X coordinate, negative is backwards, 0 means no movement
        wpose.position.x += scale * deltaX
        #same for Y
        wpose.position.y += scale * deltaY
        #same for Z
        wpose.position.z += scale * deltaZ

        #return the new positions to be added to the waypoints list
        return wpose

    #as stated above, my new method add_waypoint takes in 3 new parameters, deltaX, deltaY, and deltaZ, which will be the change 
    #in XYZ position of the EE respectively to reach the new waypoint
    #This method plan_cartesian_path will plan a cartesian path to draw an initial utilizing those waypoints in a list
    #it takes in 3 inputs in addition to the self, waypoint_deltaXs, waypoint_deltaYs, and waypoint_deltaZs
    #These are lists that contain all the deltas in order of the waypoints
    #for example, waypoint_deltaXs[0] is the deltaX for the 0th waypoint and waypoint_deltaYs[0] is the deltaY 0th waypoint etc.
    #This way I can input in different lists for waypoint_deltaXs, Ys and Zs when I call the plan_cartesian_path function and it can draw different letters
    #based on the waypoint lists i give as inputs
    def plan_cartesian_path(self, waypoint_deltaXs, waypoint_deltaYs, waypoint_deltaZs):
        # Copy class variables to local variables
        move_group = self.move_group

        ## BEGIN plan_cartesian_path
        ##
        ## Cartesian Paths
        ## ^^^^^^^^^^^^^^^
        ## You can plan a Cartesian path directly by specifying a list of waypoints
        ## for the end-effector to go through. 
        ## list to collect waypoints
        waypoints = []
        #get the current pose
        wpose = move_group.get_current_pose().pose


        #this command will loop through all the lists (which should all be the same length) and get the deltaX, Y and Z for each waypoint from the 1st waypoint (index 0)
        #to the last waypoint (index len(waypoint_deltaXs)) (Ys and Zs should give the same length)
        #the for loop defines i as the loop variable, so everytime we loop through, "i" will go from 0 to len(waypoint_deltaXs) - 1
        #Everytime it loops through, the waypoints list defined earlier will collect a new waypoint by calling my add_waypoint method and giving in the respective
        #DeltaX, DeltaY or deltaZ as input from the waypoint_delta lists
        #my add_waypoint function then returns the new waypoint which is then copied and put in the waypoints list
        for i in range(len(waypoint_deltaXs)):
            waypoints.append(copy.deepcopy(Midterm().add_waypoint(wpose=wpose,deltaX=waypoint_deltaXs[i],deltaY=waypoint_deltaYs[i],deltaZ=waypoint_deltaZs[i])))
        
        # specified 0.1 as the eef_step in Cartesian translation because it was too jerky and stopping too often with 0.01
        # We will disable the jump threshold by setting it to 0.0,
        # ignoring the check for infeasible jumps in joint space
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.1, 0.0  # waypoints to follow  # eef_step
        )  # jump_threshold

        # Note: We are just planning, not asking move_group to actually move the robot yet:
        return plan, fraction

        ## END_SUB
    
    def execute_plan(self, plan):
        # Copy class variables to local variables
        move_group = self.move_group

        ## BEGIN_SUB execute_plan
        ##
        ## Executing a Plan
        ## ^^^^^^^^^^^^^^^^
        ## Use execute if you would like the robot to follow
        ## the plan that has already been computed:
        move_group.execute(plan, wait=True)

        ## **Note:** The robot's current joint state must be within some tolerance of the
        ## first waypoint in the `RobotTrajectory`_ or ``execute()`` will fail
        ## END_SUB


def main():
    try:
        #Introduction
        print("")
        print("----------------------------------------------------------")
        print("Initials are 'A' and then 'H' and then 'T' which will be drawn using Cartesian paths" )
        print("----------------------------------------------------------")
        print("Press Ctrl-D to exit at any time")
        print("")
        input(
            "============ Press `Enter` to begin by setting up the moveit_commander ..."
        )
        #initialize
        midterm = Midterm()

        #Execute the go to joint state method for the robot to go to default joint state for drawing letters
        input(
            "============ Press `Enter` to execute default joint state goal ..."
        )
        midterm.go_to_joint_state()

        #Execute the plan cartesian path method to plan out the cartesian path to draw the first letter, 'A'
        input("============ Press `Enter` to plan and display a Cartesian path to draw 'A'...")
        #                                         wp1    wp2  wp3    wp4     wp5 
        #waypoint_deltaXs to draw the letter_A = [ 0.0,  0.0, 0.0,   0.0,    0.0]
        #waypoint_deltaYs to draw the letter A = [-0.25, 0.2, 0.1,   0.2,  -0.25]
        #waypoint_deltaZs to draw the letter A = [-0.25, 0.2, 0.0,  -0.2,   0.25]

        #first waypoint [0.0, -0.25, -0.25] moves the end effector down and to the left to draw the left slash of 'A'
        #second waypoint [0.0, 0.2, 0.2] moves the end effector back up and to the right to get back to the middle 
        #third waypoint [0.0, 0.1, 0.0] moves the end effector straight to the right to draw the middle line of the 'A'
        #forth waypoint [0.0, 0.2, -0.2] moves the end effector down and to the right to draw part of the right slash from the middle of the 'A'
        #fifth waypoint [0.0, -0.25, 0.25] finishes the 'A' drawing the entire right slash by going up and to the left 
        cartesian_plan, fraction = midterm.plan_cartesian_path(waypoint_deltaXs=  [ 0.0,  0.0,  0.0,  0.0,  0.0],
                                                                 waypoint_deltaYs=[-0.25, 0.2,  0.1,  0.2, -0.25],
                                                                 waypoint_deltaZs=[-0.25, 0.2,  0.0, -0.2,  0.25])

        #Execute the execute plan method to execute the cartesian path given above
        input("============ Press `Enter` to execute a cartesian path to draw 'A' ...")
        midterm.execute_plan(cartesian_plan)

        #Execute the go to joint state method for the robot to go to default joint state
        input(
            "============ Press `Enter` to execute default joint state goal ..."
        )
        midterm.go_to_joint_state()

        #Execute the plan cartesian path method to plan out the cartesian path to draw the second letter, 'H'
        input("============ Press `Enter` to plan and display a Cartesian path to draw 'H'...")
        #                                          wp1   wp2  wp3  wp4   wp5    wp6  wp7 
        #waypoint_deltaXs to draw the letter_H = [ 0.0,  0.0, 0.0, 0.0,  0.0,  0.0,  0.0]
        #waypoint_deltaYs to draw the letter H = [-0.1,  0.0, 0.0, 0.0, 0.15,  0.0,  0.0]
        #waypoint_deltaZs to draw the letter H = [ 0.0, 0.05,-0.2, 0.1,  0.0, 0.05, -0.2]

        #first waypoint [0.0, -0.1, 0.0] moves the end effector to the left to get to the left side to draw the 'H'
        #second waypoint [0.0, 0.0, 0.05] moves the end effector up to draw the left top line above the middle of the 'H' 
        #third waypoint [0.0, 0.0, -0.2] moves the end effector down to create the complete left line of the 'H'
        #forth waypoint [0.0, 0.0, 0.1] moves the end effector back up to the intersection of the left line and the middle line of the 'H'
        #fifth waypoint [0.0, 0.15, 0.0] moves to the right to create the middle line of the 'H'
        #sixth waypoint [0.0, 0.0, 0.05] moves the end effector up again to draw the right top line above the middle of the 'H'
        #seventh waypoint [0.0, 0.0, -0.2] moves the end effector down to finish the 'H' by drawing the complete right line
        cartesian_plan, fraction = midterm.plan_cartesian_path(waypoint_deltaXs=  [0.0,   0.0,  0.0, 0.0,  0.0,  0.0,  0.0],
                                                                 waypoint_deltaYs=[-0.1,  0.0,  0.0, 0.0, 0.15,  0.0,  0.0],
                                                                 waypoint_deltaZs=[ 0.0, 0.05, -0.2, 0.1,  0.0, 0.05, -0.2])

        #Execute the execute plan method to execute the cartesian path given above
        input("============ Press `Enter` to execute a cartesian path to draw 'H' ...")
        midterm.execute_plan(cartesian_plan)

        #Execute the go to joint state method for the robot to go to default joint state
        input(
            "============ Press `Enter` to execute default joint state goal ..."
        )
        midterm.go_to_joint_state()

        #Execute the plan cartesian path method to plan out the cartesian path to draw the third letter, 'T'
        input("============ Press `Enter` to plan and display a Cartesian path to draw 'T'...")
        #                                          wp1   wp2  wp3  wp4   wp5   
        #waypoint_deltaXs to draw the letter_T = [ 0.0,  0.0, 0.0, 0.0,  0.0]
        #waypoint_deltaYs to draw the letter T = [ 0.15, -0.15, 0.0, 0.0, -0.1]
        #waypoint_deltaZs to draw the letter T = [ 0.0,  0.0,-0.15, 0.15,  0.0]

        #first waypoint [0.0, 0.15, 0.0] moves the end effector to the right corner to draw the right half of the 'T' line
        #second waypoint [0.0, -0.15, 0.0] moves the end effector back to the left to get it in position to draw the middle vertical line 
        #third waypoint [0.0, 0.0, -0.15] moves the end effector down to create the complete middle line of the 'T'
        #forth waypoint [0.0, 0.0, 0.15] moves the end effector back up to get it in position to complete the left half of the 'T' line
        #fifth waypoint [0.0, -0.1, 0.0] moves to the left to the left corner of the 'T' to finish the horizontal line 
        cartesian_plan, fraction = midterm.plan_cartesian_path(waypoint_deltaXs=  [  0.0,   0.0,  0.0,  0.0,   0.0],
                                                                 waypoint_deltaYs=[ 0.15, -0.15,  0.0,  0.0, -0.1],
                                                                 waypoint_deltaZs=[  0.0,   0.0,-0.15, 0.15,   0.0])

        #Execute the execute plan method to execute the cartesian path given above
        input("============ Press `Enter` to execute a cartesian path to draw 'T' ...")
        midterm.execute_plan(cartesian_plan)

        print("============ Python execution complete!")
    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return


if __name__ == "__main__":
    main()

